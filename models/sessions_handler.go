package models

import (
	"errors"
	"strings"

	"github.com/ezanirvana/messenger-backend/services"
	"github.com/twinj/uuid"
)

type sessionsHandler struct {
	PlatformRE string
}

func NewSessionsHandler() *sessionsHandler {
	return &sessionsHandler{}
}

func (h *sessionsHandler) Create(userID string, build int, name, remoteAddr string) (*Session, error) {
	s := &Session{
		ID:          uuid.NewV4().String(),
		UserID:      userID,
		Build:       build,
		Name:        name,
		AccessToken: uuid.NewV4().String(),
		CreatedAt:   services.Timestamp(),
		UpdatedAt:   services.Timestamp(),
	}

	if s.Build == 0 {
		return nil, errors.New("build invalid")
	}

	s.IPAddress = strings.Split(remoteAddr, ":")[0]

	if err := s.Create(); err != nil {
		return nil, err
	}

	return s, nil

}
