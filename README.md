### REGISTRATION
```
POST
http://localhost:3000/users
Header `content-type: application/json`
{
    "username": "<username>",
    "email": "<email>",
    "password": "<password>"
}

You will get the user id

{
    "id": "<id>"
}
```

### LOGIN
```
POST
http://localhost:3000/login
Header `content-type: application/json`
{
    "username": "<username>",
    "email": "<email>",
    "password": "<password>"
}

You will get the token

{
    "token": "<token>"
}
```

### CREATE SESSION
```
POST
http://localhost:3000/sessions
Header `authorization: bearer <token>
Header `content-type: application/json`
{
    "build": <session build number, only int>,
    "name": "<session name>"
}

This endpoint will send you the id and messaging_url to take a chat session

{
    "created_at": <time.created_at>,
    "id": "<session_id>",
    "messaging_url": "<ws url to connect to the ws server, example : "ws://localhost:3000/d1d0c2bf-66bd-4376-973b-aa0479fab043">"
}
```

### CONNECT TO SERVER
```javascript
var socket = new WebSocket("<ws url>");
socket.onopen = function() {
   socket.send(JSON.stringify({"method":20, "timestamp":new Date().getTime()}));
};
socket.onmessage = function(event) {
  console.log(event.data);
};
```

### CREATE GROUP
- "user_ids" property is will be assign to group
```
POST
http://localhost:3000/groups
Header `authorization: bearer <user's token who want to create group>`
Header `content-type: application-json`
{
    "name":"<group name>",
    "user_ids": ["<the user you invite to group>"]
}

{
    "id":"<group id>"
}
```

### RPC Message Send
```javascript
    socket.send(JSON.stringify({"method":40,"body":{"group_id":"<group id>", "data":"<your message>"}}));
```

### RPC Message Delivered
```javascript
    socket.send(JSON.stringify({"method":41,"body":{"message_id":"<message id>"}}));
```

### RPC Message Read
```javascript
    socket.send(JSON.stringify({"method":42,"body":{"message_id":"<message id>"}}));
```

### JOIN GROUP
```
PUT
http://localhost:3000/groups/<group id>/join
Header `authorization: bearer <user's token who want to join group>`
Header `content-type: application-json`
{
    "user_id":"<user id>"
}
```