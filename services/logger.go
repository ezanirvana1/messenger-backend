package services

import (
	"go.uber.org/zap"
)

var zapLogger, _ = zap.NewDevelopment()

var Logger = zapLogger.Sugar()