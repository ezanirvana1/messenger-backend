package rpc

import (
	"github.com/ezanirvana/messenger-backend/messaging/client"
	"github.com/ezanirvana/messenger-backend/messaging/events"
	"github.com/ezanirvana/messenger-backend/models"
	"github.com/ezanirvana/messenger-backend/protocol"
	"github.com/ezanirvana/messenger-backend/services"
)

type messages struct{}

func newMessages() *messages {
	return &messages{}
}

func (m *messages) Send(c *client.Client, p *protocol.RpcMessageSend) {
	withGroup(p.GroupID, c.UserID, func(group *models.Group) {
		msg, _ := models.Messages.Create(p.GroupID, c.UserID, p.Data, services.Timestamp())

		e := events.NewMessage(msg)
		e.SaveForUsers(msg.ID, group.UserIDs)

		es := events.NewMessageSent(msg.ID, e.Timestamp)
		es.SaveForUser(msg.ID, group.UserID)
		es.SendToUser(group.UserID)

		e.SendToUsersWithoutMe(c.SessionID, group.UserIDs)
	})
}

func (m *messages) Delivered(c *client.Client, p *protocol.RpcMessageDelivered) {
	msg, err := models.Messages.ByID(p.MessageID)
	if err != nil {
		return
	}

	withGroup(msg.GroupID, c.UserID, func(group *models.Group) {
		e := events.NewMessageDelivered(msg.ID, services.Timestamp())
		e.SaveForUser(msg.ID, msg.UserID)
		e.SendToUsersWithoutMe(c.SessionID, []string{msg.UserID, c.UserID})
		e.DeleteOldEvents(msg.ID)
	})
}

func (m *messages) Read(c *client.Client, p *protocol.RpcMessageRead) {
	msg, err := models.Messages.ByID(p.MessageID)
	if err != nil {
		return
	}

	withGroup(msg.GroupID, c.UserID, func(group *models.Group) {
		e := events.NewMessageRead(msg.ID, services.Timestamp())
		e.SaveForUser(msg.ID, msg.UserID)
		e.SendToUsersWithoutMe(c.SessionID, []string{msg.UserID, c.UserID})
		e.DeleteOldEvents(msg.ID)
	})
}
