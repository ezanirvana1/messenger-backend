package main

import (
	"net/http"

	"github.com/ant0ine/go-json-rest/rest"
	"github.com/ezanirvana/messenger-backend/controllers"
	"github.com/ezanirvana/messenger-backend/services"
	"github.com/ezanirvana/messenger-backend/messaging"
)

var (
	users = controllers.NewUserController()
	sessions = controllers.NewSessionsController()
	groups = controllers.NewGroupController()
)

func main() {
	api := rest.NewApi()
	api.Use(rest.DefaultDevStack...)
	r, err := rest.MakeRouter(
		//LOGIN USER
		rest.Post("/login", users.Login),
		//REGISTER USER
		rest.Post("/users", users.Register),
		//CREATE SESSION
		rest.Post("/sessions", sessions.Create),
		//CREATE GROUP
		rest.Post("/groups", groups.Create),
		//JOIN GROUP
		rest.Put("/groups/:id/join", groups.Join),
		//MESSAGING
		rest.Get("/:access_token", messaging.Start),
	)
	if err != nil {
		services.Logger.Fatal(err)
	}
	api.SetApp(r)
	http.ListenAndServe(":3000", api.MakeHandler())
}